import image1 from './assets/city-skyline.jpg';
import image2 from './assets/cn-tower.jpg';
import image3 from './assets/new-city-hall.jpg';
import image4 from './assets/old-city-hall.jpg';
import image5 from './assets/rogers-center.jpg';
import image6 from './assets/rom.jpg';
import facebook from './assets/facebook-square-brands.svg';
import twitter from './assets/twitter-square-brands.svg';
import instagram from './assets/instagram-square-brands.svg';
import linkedin from './assets/linkedin-brands.svg'
import './App.css';
import { useState } from 'react';


function App() {
  const images = [
    {photo:image1, title: "Toronto", desc: "The Skyline"},
    {photo:image2, title: "The CN Tower", desc: "One of the worlds tallest towers."},
    {photo:image5, title: "The Rogers Centre", desc: "Toronto's iconic Skydome."},
    {photo:image4, title: "Old City Hall", desc: "The Clock Tower"},
    {photo:image6, title: "Royal Ontario Museum", desc: "Most visited museum in Canada."},
    {photo:image3, title: "Toronto City Hall", desc: "A brutalist architecture masterpiece."},
    
  ];

  const socialIcons = [facebook, twitter, instagram, linkedin];
  const [selectedImage, setSelectedImage] = useState(null);
  
  
    return (
      <div className="App">
        <header className="App-header">
      
       <h1>
            The City of <strong>Toronto</strong>
          </h1>
          <h2>Historical Sites & <strong>More</strong></h2>
        
        </header>
   
        <section class="grid">
          { images.map((image) => (
         <div key={image.photo} class="item">
        <div class="image-overlay-copy">
<h3>
  {image.title}
</h3>
<p>
  {image.desc}
</p>
</div>
         <div class="image-overlay"> </div >
         
            <img src={image.photo} 
            alt="logo" 
            onClick={()=>setSelectedImage(image.photo)} />
          </div>
          
            ))}
           
        </section>
     
        <section class="footer">
         
       
         <p>
         <a href="#"> <img src={twitter } class="social-icons" /> </a>
         <a href="#"> <img src={facebook } class="social-icons" /> </a>
         <a href="#"> <img src={linkedin } class="social-icons" /> </a>
         <a href="#">  <img src={instagram} class="social-icons" /> </a>
         
         
         
        

         </p>
         </section>
          
          
      
           
        
        
   
        <div id='overlay' style={{visibility: selectedImage ? 'visible': 'hidden'}}>
          <h4><a class="close" onClick={ ()=>setSelectedImage(null) }>X</a></h4>
          <img src={selectedImage} />
        </div>
       
      </div>
    
         
      
    );
  }
  
  export default App;
  
  